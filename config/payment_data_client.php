<?php

return [
    'http_errors' => false,
    'base_uri' => env('PAYMENT_DATA_URL'),
];
